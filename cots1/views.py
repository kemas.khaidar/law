from django.shortcuts import render

import requests
import json

# Create your views here.
def index(request):
    response = {}
    if (request.method == 'POST'):
        try:
            param1 = int(request.POST['param1'])
            param2 = int(request.POST['param2'])
            response['param1'] = param1
            response['param2'] = param2
            response['jumlah'] = param1 + param2
            response['kurang'] = param1 - param2
            response['kali'] = param1 * param2
        except:
            response['error'] = "parameter ada yang bukan integer"
    return render(request, 'cots1.html', response)