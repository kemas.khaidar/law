from django.conf.urls import url
from django.views.generic.base import RedirectView
from .views import *
#url for app
app_name = 'cots1'

urlpatterns = [
	url(r'^$', RedirectView.as_view(url='cots1/', permanent = False), name='$'),
    url(r'cots1/', index, name='index'),
]
