from django.conf.urls import url
from django.views.generic.base import RedirectView
from .views import *
#url for app
app_name = 'lab_2'

urlpatterns = [
	url(r'^$', RedirectView.as_view(url='weather/', permanent = False), name='$'),
    url(r'weather/', index, name='index'),
]
