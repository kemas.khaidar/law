from django.shortcuts import render
import requests
import json

# Create your views here.
def index(request):
    response = {}
    if (request.method == 'POST'):
        kota = request.POST['kota']
        if (kota == ""):
            return render(request, 'home.html', response)
        url = "https://www.metaweather.com/api/location/search/?query=" + str(kota)
        resp = requests.get(url)
        content = json.loads(resp.text)
        listItems = []
        for item in content:
            woeid = item['woeid']
            specificUrl = "https://www.metaweather.com/api/location/" + str(woeid)
            specificResp = requests.get(specificUrl)
            specificContent = json.loads(specificResp.text)
            listItems.append(specificContent)
        response['items'] = listItems
    return render(request, 'home.html', response)